<?= view('layouts/header'); ?>
<?= view('layouts/navbar'); ?>

<main>
    <!-- Alternative 2 Heading -->
    <header class="header alter2-header section" id="home">
        <div class="shapes-container">
            <!-- diagonal shapes -->
            <div class="shape shape-animated" data-aos="fade-down-right" data-aos-duration="1500" data-aos-delay="100"></div>
            <div class="shape shape-animated" data-aos="fade-down" data-aos-duration="1000" data-aos-delay="100">
            </div>
            <div class="shape shape-animated" data-aos="fade-up-left" data-aos-duration="500" data-aos-delay="200">
            </div>
            <div class="shape shape-animated" data-aos="fade-up" data-aos-duration="500" data-aos-delay="200"></div>
            <div class="shape shape-animated" data-aos="fade-up-right" data-aos-duration="1000" data-aos-delay="200"></div>
            <div class="shape shape-animated" data-aos="fade-up" data-aos-duration="1000" data-aos-delay="200">
            </div>
            <div class="shape shape-animated" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
            <div class="shape shape-animated" data-aos="fade-down-left" data-aos-duration="1000" data-aos-delay="100"></div>
            <div class="shape shape-animated" data-aos="zoom-in" data-aos-duration="1000" data-aos-delay="300">
            </div><!-- animated shapes -->
            <div class="animation-shape shape-ring animation--clockwise"></div>
            <div class="animation-shape shape-circle shape-circle-1 animation--anti-clockwise"></div>
            <div class="animation-shape shape-circle shape-circle-2 animation--clockwise"></div>
            <div class="animation-shape shape-heart animation--clockwise">
                <div class="animation--rotating"></div>
            </div>
            <div class="animation-shape shape-triangle animation--rotating-diagonal">
                <div class="animation--rotating"></div>
            </div>
            <div class="animation-shape shape-diamond animation--anti-clockwise">
                <div class="animation--rotating"></div>
            </div><!-- static shapes -->
            <div class="static-shape shape-ring-1"></div>
            <div class="static-shape shape-ring-2"></div>
            <div class="static-shape shape-circle shape-circle-1">
                <div data-aos="fade-down-left"></div>
            </div>
            <div class="static-shape shape-circle shape-circle-2">
                <div data-aos="fade-down-left" data-aos-delay="500"></div>
            </div>
            <div class="static-shape pattern-dots-1"></div>
            <div class="static-shape pattern-dots-2"></div><!-- main shape -->
            <div class="static-shape background-shape-main"></div><!-- ghost shapes -->
            <div class="static-shape ghost-shape ghost-shape-1"></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <h1 class="display-4 display-md-6 mt-3"><span class="bold">Nafisha Universal Network</span></h1>
                    <p class="lead bold text-primary">About Us</span></p>
                    <p class="lead">Nafisha Universal Network was established in 2021 in Jakarta, Indonesia. We started as an IT service provider such as software development, Operation and system integration. We focusing in only IT area as our team members are Expert in Software Development. Several Project has been taken by our teams with satisfied results from our clients.</p>
                </div>
                <div class="col-md-6">
                    <img src="<?= base_url("public/assets/img/dev.svg") ?>" style="width: 500px;" alt="...">
                </div>
            </div>
        </div>
    </header><!-- Partners Slider -->

    <section class="section alter3-features" id="features">
        <div class="shapes-container">
            <div class="shape shape-ring shape-ring-1">
                <div class="animation animation--rotating"></div>
            </div>
            <div class="shape shape-ring shape-ring-2">
                <div class="animation animation--rotating"></div>
            </div>
            <div class="shape shape-circle animation--clockwise">
                <div></div>
            </div>
            <div class="shape background-shape-main"></div>
        </div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-5 order-lg-last">
                    <div class="section-heading">
                        <h3 class="heading-line">Today as Company build and on going for 1 years, we keep on track to be the better service solution in IT Area.</h3>
                    </div>
                </div>
                <div class="col-lg-7 pr-lg-6">
                    <div class="row">
                        <div class="col-lg-6 rotated-cards">
                            <div data-aos="fade-down" data-aos-delay="0" class="aos-init aos-animate">
                                <div class="card border-0 shadow mb-5 tilt">
                                    <div class="card-body py-5 px-4">
                                        <div class="icon-box rounded-circle gradient gradient-primary-light text-contrast shadow icon-xl mb-3"><i class="icon m-0 pe pe-7s-target pe-3x"></i></div>
                                        <h4 class="bold mb-5">Objective</h4>
                                        <p class="text-muted lead">Meet customer requirements, by Serving Development and operation in IT Area with Good Quality ,24 hours 7 days .</p>
                                    </div>
                                    <div class="js-tilt-glare" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none;">
                                        <div class="js-tilt-glare-inner" style="position: absolute; top: 50%; left: 50%; background-image: linear-gradient(0deg, rgba(255, 255, 255, 0) 0%, rgb(255, 255, 255) 100%); width: 523.984px; height: 523.984px; transform: rotate(180deg) translate(-50%, -50%); transform-origin: 0% 0%; opacity: 0;"></div>
                                    </div>
                                </div>
                            </div>
                            <div data-aos="fade-down" data-aos-delay="500" class="aos-init">
                                <div class="card border-0 shadow mb-5 tilt">
                                    <div class="card-body py-5 px-4">
                                        <div class="icon-box rounded-circle gradient gradient-primary-light text-contrast shadow icon-xl mb-3"><i class="icon m-0 pe pe-7s-glasses pe-3x"></i></div>
                                        <h4 class="bold mb-5">Vision</h4>
                                        <p class="text-muted lead">Together move forward with our clients and pursue the best quality of services.</p>
                                    </div>
                                    <div class="js-tilt-glare" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none;">
                                        <div class="js-tilt-glare-inner" style="position: absolute; top: 50%; left: 50%; background-image: linear-gradient(0deg, rgba(255, 255, 255, 0) 0%, rgb(255, 255, 255) 100%); width: 523.984px; height: 523.984px; transform: rotate(180deg) translate(-50%, -50%); transform-origin: 0% 0%; opacity: 0;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 rotated-cards">
                            <div data-aos="fade-down" data-aos-delay="800" class="aos-init aos-animate">
                                <div class="card border-0 shadow mb-5 tilt" style="will-change: transform; transform: perspective(300px) rotateX(0deg) rotateY(0deg);">
                                    <div class="card-body py-5 px-4">
                                        <div class="icon-box rounded-circle gradient gradient-primary-light text-contrast shadow icon-xl mb-3"><i class="icon m-0 pe pe-7s-rocket pe-3x"></i></div>
                                        <h4 class="bold mb-5">Mission</h4>
                                        <p class="text-muted lead">Talent Acquisition and Strengthen thru the nations with IT area as Domain.</p>
                                    </div>
                                    <div class="js-tilt-glare" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none;">
                                        <div class="js-tilt-glare-inner" style="position: absolute; top: 50%; left: 50%; background-image: linear-gradient(0deg, rgba(255, 255, 255, 0) 0%, rgb(255, 255, 255) 100%); width: 523.984px; height: 523.984px; transform: rotate(180deg) translate(-50%, -50%); transform-origin: 0% 0%; opacity: 0;"></div>
                                    </div>
                                </div>
                            </div>
                            <div data-aos="fade-down" data-aos-delay="1200" class="aos-init">
                                <div class="card border-0 shadow mb-5 tilt">
                                    <div class="card-body py-5 px-4">
                                        <div class="icon-box rounded-circle gradient gradient-primary-light text-contrast shadow icon-xl mb-3"><i class="icon m-0 pe pe-7s-global pe-3x"></i></div>
                                        <h4 class="bold mb-5">Core Values</h4>
                                        <p class="text-muted lead">Customer intimacy, Quality, and Delivery.</p>
                                    </div>
                                    <div class="js-tilt-glare" style="position: absolute; top: 0px; left: 0px; width: 100%; height: 100%; overflow: hidden; pointer-events: none;">
                                        <div class="js-tilt-glare-inner" style="position: absolute; top: 50%; left: 50%; background-image: linear-gradient(0deg, rgba(255, 255, 255, 0) 0%, rgb(255, 255, 255) 100%); width: 523.984px; height: 523.984px; transform: rotate(180deg) translate(-50%, -50%); transform-origin: 0% 0%; opacity: 0;"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="section features-cant-miss" id="services">
        <div class="shapes-container overflow-clear">
            <div class="shape shape-circle shape-circle-1">
                <div data-aos="fade-up-left"></div>
            </div>
            <div class="shape shape-circle shape-circle-2">
                <div data-aos="fade-up-right" data-aos-delay="200"></div>
            </div>
            <div class="shape shape-circle shape-circle-3">
                <div data-aos="fade-up-left" data-aos-delay="400"></div>
            </div>
            <div class="shape shape-circle shape-circle-4">
                <div data-aos="fade-up-left" data-aos-delay="600"></div>
            </div>
            <div class="shape shape-triangle shape-animated">
                <div class="animation--rotating"></div>
            </div>
        </div>
        <div class="container">
            <div class="row gap-y">
                <div class="col-md-6 order-md-last">
                    <div class="section-heading">
                        <h2 class="heading-line">Services</h2>
                    </div>
                    <ul class="list-unstyled">
                        <li class="media flex-column flex-md-row text-center text-md-left"><i class="mx-auto mr-md-3 mx-auto mr-md-3 accent pe pe-7s-headphones pe-3x"></i>
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="bold mt-0 mb-1">Consultancy</h5>
                                <p class="m-0">As our tagline to be closest with our clients, we proposing a new different way of IT Consultancy. Not only put the mainstream jargon, but also we give more value to our clients. Hence, our clients has value in more area.</p>
                            </div>
                        </li>
                        <li class="media flex-column flex-md-row text-center text-md-left mt-4"><i class="mx-auto mr-md-3 mx-auto mr-md-3 accent pe pe-7s-server pe-3x"></i>
                            <div class="media-body mt-3 mt-md-0">
                                <h5 class="bold mt-0 mb-1">System Design, Devops</h5>
                                <p class="m-0">We offer computer system design, development and operation as Full-stack domain. With our team members expertise in areas like Front Stack such Java Script technology, and Back end such Java, MySQL. We give more, not only a piece of applications, but also beyond of applications itself, like seam-less transfer knowledge,
                                    hand and legs extension, etc.</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-6">
                    <img src="<?= base_url("public/assets/img/service.svg") ?>" alt="...">
                </div>
            </div>
        </div>
    </section>


    <section class="section pricing-plans" id="portofolio">
        <div class="shapes-container overflow-clear">
            <div class="shape shape-1"></div>
            <div class="shape shape-2"></div>
            <div class="shape shape-3"></div>
            <div class="shape shape-4"></div><i class="icon pe pe-7s-cash pe-3x" data-aos="fade-down"></i> <i class="icon fas fa-dollar-sign fa-3x" data-aos="fade-up"></i> <i class="icon pe pe-7s-piggy pe-3x" data-aos="fade-up"></i> <i class="icon pe pe-7s-cart pe-3x" data-aos="fade-left"></i> <i class="icon far fa-credit-card fa-3x" data-aos="fade-down"></i> <i class="icon far fa-money-bill-alt fa-3x" data-aos="fade-up"></i>
        </div>
        <div class="container">
            <div class="section-heading text-center">
                <h2 class="mt-3 heading-line">Portofolio</h2>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <img src="<?= base_url("public/assets/img/sms.svg") ?>" style="width: 480px; margin-top: -46px;" alt="...">
                </div>
                <div class="col-md-6">
                    <div class="section-heading">
                        <h3 class="heading-line">Advance SMPP asynchronous in Twilio Project with Mustika Solution</h3>
                        <p class="lead text-muted">With switch trend from P to P SMS to become A to P SMS, this area growth and required more advance solution, in terms of service and agile delivery. Facebook, Whatapps and up to domestic Partner require an Agile SMS platform with high throughput and High continuity integration. We offer new models of SMPP method, with custom solution
                            by adopt async process instead of normal way (sync process), this is give solution between twilio, telin and MAZ / Mustika (one of Telkomsel Digiads Partner) to overcome High latency issue</p>
                    </div>
                </div>
            </div>

            <div class="row" style="padding-top: 110px;">
                <div class="col-md-6">
                    <div class="section-heading">
                        <h3 class="heading-line">Standardize IoT Gateway using MQTT</h3>
                        <p class="lead text-muted">As Growth of IoT Business trends nowadays, our team members build a new way of IoT Device integration with new model / methodology, while the normal way need normalize its protocols first in each of device, with our solution any device can connect to central / service gateway without normalize any protocols previously.</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <img src="<?= base_url("public/assets/img/iot.svg") ?>" style="width: 580px;" alt="...">
                </div>
            </div>
        </div>
    </section>

    <section class="section" id="ourClients">
        <div class="container py-5 border-bottom">
            <div class="section-heading text-center">
                <h2 class="mt-3 heading-line">Our Clients</h2>
            </div>
            <div class="swiper-container swiper-container-horizontal" data-sw-show-items="5" data-sw-space-between="30" data-sw-autoplay="2500" data-sw-loop="true">
                <div class="swiper-wrapper align-items-center" style="transition-duration: 0ms; transform: translate3d(-192px, 0px, 0px);">
                    <div class="swiper-slide swiper-slide-next" data-swiper-slide-index="1" style="width: 34px; margin-right: 30px;"><img src="<?= base_url("public/assets/img/agogo.jpeg") ?>" class="img-responsive" alt="" style="max-height: 60px"></div>
                    <div class="swiper-slide" data-swiper-slide-index="2" style="width: 34px; margin-right: 30px;"><img src="<?= base_url("public/assets/img/twillio.jpeg") ?>" class="img-responsive" alt="" style="max-height: 60px"></div>
                    <div class="swiper-slide" data-swiper-slide-index="3" style="width: 34px; margin-right: 30px;"><img src="<?= base_url("public/assets/img/rumah-telin.jpeg") ?>" class="img-responsive" alt="" style="max-height: 60px"></div>
                    <div class="swiper-slide" data-swiper-slide-index="4" style="width: 34px; margin-right: 30px;"><img src="<?= base_url("public/assets/img/telin.jpeg") ?>" class="img-responsive" alt="" style="max-height: 60px"></div>
                    <div class="swiper-slide" data-swiper-slide-index="4" style="width: 34px; margin-right: 30px;"><img src="<?= base_url("public/assets/img/go.png") ?>" class="img-responsive" alt="" style="max-height: 60px"></div>
                    <div class="swiper-slide" data-swiper-slide-index="4" style="width: 34px; margin-right: 30px;"><img src="<?= base_url("public/assets/img/maz.png") ?>" class="img-responsive" alt="" style="max-height: 60px"></div>
                </div><span class="swiper-notification" aria-live="assertive" aria-atomic="true"></span>
            </div>
        </div>
    </section>

    <section class="section why-choose-us" id="contactUs">
        <div class="shapes-container">
            <div class="pattern pattern-dots"></div>
        </div>
        <div class="container pb-8 bring-to-front">
            <div class="section-heading text-center">
                <h2 class="heading-line">Contact US</h2>
                <p class="text-muted lead mx-auto">Hi, we are looking forward to start a project with you.
                    Send us a message and we will be
                    in touch within one business day.</p>
            </div>
            <div class="row gap-y">
                <div class="col-md-6">
                    <ul class="list-unstyled why-icon-list">
                        <li class="list-item">
                            <div class="media align-items-center">
                                <div class="icon-shape mr-3">
                                    <div class="shape shape-pipes"></div><i class="icon text-alternate fas fa-envelope fa-3x"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bold">Email</h5>
                                    <p class="my-0">nafishauniversalnetwork@nuncorp.id</p>
                                </div>
                            </div>
                        </li>
                        <li class="list-item">
                            <div class="media align-items-center">
                                <div class="icon-shape mr-3">
                                    <div class="shape shape-pipes"></div><i class="icon text-alternate fas fa-phone fa-3x"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bold">Phone</h5>
                                    <p class="my-0">+62 813 1932 2225</p>
                                </div>
                            </div>
                        </li>
                        <li class="list-item">
                            <div class="media align-items-center">
                                <div class="icon-shape mr-3">
                                    <div class="shape shape-pipes"></div><i class="icon text-alternate fas fa-globe fa-3x"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bold">Website</h5>
                                    <p class="my-0">www.nuncorp.id</p>
                                </div>
                            </div>
                        </li>
                        <li class="list-item">
                            <div class="media align-items-center">
                                <div class="icon-shape mr-3">
                                    <div class="shape shape-pipes"></div><i class="icon text-alternate fas fa-map-marker-alt fa-3x"></i>
                                </div>
                                <div class="media-body">
                                    <h5 class="bold">Address</h5>
                                    <p class="my-0">
                                        Taman Manggis Permai Blok J No. 4 Kel. Sukamaju, Kec. Cilodong
                                        Depok 16415</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <hr class="my-5">
                </div>
                <div class="col-md-6">
                    <div class="rotated-mockups device-twin">
                        <img src="<?= base_url("public/assets/img/contact.svg") ?>" style="width: 580px;" alt="...">
                    </div>
                </div>
            </div>
        </div>
    </section>
</main>

<?= view('layouts/footer'); ?>
<?= view('layouts/script'); ?>