<section class="section features" id="features">
        <div class="container pt-0" style="max-width: 1312px;">
            <div class="bg-contrast shadow rounded p-5">
                <div class="row gap-y">
                    <div class="col-md-3">
                        <div class="icon-box rounded gradient gradient-primary-light text-contrast shadow icon-xl"><i class="icon m-0 pe pe-7s-target pe-3x"></i></div>
                        <h4 class="semi-bold mt-4 text-capitalize">Objective</h4>
                        <hr class="w-25 bw-2 border-alternate mt-3 mb-4">
                        <p class="regular text-muted">Meet customer requirements, by Serving Development and operation in IT Area with Good Quality ,24 hours 7 days .</p>
                    </div>
                    <div class="col-md-3">
                        <div class="icon-box rounded gradient gradient-primary-light text-contrast shadow icon-xl"><i class="icon m-0 pe pe-7s-glasses pe-3x"></i></div>
                        <h4 class="semi-bold mt-4 text-capitalize">Vision</h4>
                        <hr class="w-25 bw-2 border-alternate mt-3 mb-4">
                        <p class="regular text-muted">Together move forward with our clients and pursue the best quality of services.</p>
                    </div>
                    <div class="col-md-3">
                        <div class="icon-box rounded gradient gradient-primary-light text-contrast shadow icon-xl"><i class="icon m-0 pe pe-7s-rocket pe-3x"></i></div>
                        <h4 class="semi-bold mt-4 text-capitalize">Mission </h4>
                        <hr class="w-25 bw-2 border-alternate mt-3 mb-4">
                        <p class="regular text-muted">Talent Acquisition and Strengthen thru the nations with IT area as Domain.</p>
                    </div>
                    <div class="col-md-3">
                        <div class="icon-box rounded gradient gradient-primary-light text-contrast shadow icon-xl"><i class="icon m-0 pe pe-7s-global pe-3x"></i></div>
                        <h4 class="semi-bold mt-4 text-capitalize">Core Values</h4>
                        <hr class="w-25 bw-2 border-alternate mt-3 mb-4">
                        <p class="regular text-muted">Customer intimacy, Quality, and Delivery.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>