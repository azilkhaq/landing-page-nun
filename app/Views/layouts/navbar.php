 <!-- Main Navigation -->
 <nav class="navbar navbar-expand-md main-nav navigation fixed-top sidebar-left">
     <div class="container"><button class="navbar-toggler" type="button"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button> <a href="<?= base_url("home") ?>" class="navbar-brand"><img src="<?= base_url("public/assets/img/logo.jpeg") ?>" alt="Laapp" class="logo logo-sticky"></a>
         <div class="collapse navbar-collapse" id="main-navbar">
             <div class="sidebar-brand"><a href="<?= base_url("home") ?>"><img src="<?= base_url("public/assets/img/logo.jpeg") ?>" alt="Laapp Template" class="logo"></a></div>
             <ul class="nav navbar-nav ml-auto">
                 <li class="nav-item"><a class="nav-link scrollto" href="#home">Home</a></li>
                 <li class="nav-item"><a class="nav-link scrollto" href="#services">Services</a></li>
                 <li class="nav-item"><a class="nav-link scrollto" href="#portofolio">Portofolio</a></li>
                 <li class="nav-item"><a class="nav-link scrollto" href="#ourClients">Our Clients</a></li>
                 <li class="nav-item"><a class="nav-link scrollto" href="#contactUs">Contact US</a></li>
             </ul>
         </div>
     </div>
 </nav>