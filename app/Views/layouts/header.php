<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>NUN | Nafisha Universal Network</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width,initial-scale=1"><!-- Place favicon.ico in the root directory -->
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <link rel="icon" href="favicon.ico">
    <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,600,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?= base_url("public/assets/css/laapp.min.css") ?>">
    <link rel="shortcut icon" href="<?= base_url('public/assets/img/logo.jpeg') ?>">
    <link rel="apple-touch-icon" href="<?= base_url('public/assets/img/logo.jpeg') ?>">
</head>