<footer class="site-footer section bg-dark text-contrast edge top-left">
    <div class="container py-3">
        <hr class="mt-5 op-5">
        <div class="row small">
            <div class="col-md-4">
                <p class="mt-2 mb-0 text-center text-md-left">© Nafisha Universal Network.
                    All Rights Reserved</p>
            </div>
        </div>
    </div>
</footer>